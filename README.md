<<<<<<< HEAD
## Demo Repo
### The repo contains the following elements
1. At root level spring-boot `perso-greet` project ( [tutorial](https://spring.io/guides/gs/serving-web-content/) ) 
   * Maven project - build using `mvn clean package`
   * Dockerfile - Packaging this project in Image  
   * Jenkinsfile - CI/CD for this project  
2. helm-package
   * Contains greet chart - basic web helm package to wrap the docker image container for k8s deployment
   * Dockerfile_helm - docker image for building the helm package  
3. deployment folder 
   * containing ansible(v2.9) playbooks and roles 
     - Setup the Jesnkins Master server
     - Setup the k8s (minikube) deployment server
     - Deploying the app via helm to k8s (localhost minikube)
     - Starting + Stopping port-forward from remote to the minikube service 
4. environment folder 
   * Containing the info to access both servers 
    
### Status
1. `perso-greet` project (maven) is building successfully tested and verified (Treat this as a black box).
2. The `perso-greet` Project Image is also builds correctly and tested locally (Treat this as a black box).
3. Jenkinsfile was tested in a previous environment which has been compromised
4. deployment 
   1. deploy_jenkins playbook deploying jenkins master
   2. deploy_minikube deploying the runtime environment
   3. deploy_app_to_minikube.yml deploy the app to minikube

>#### Currently, The code was compromised. Thus, Jenkinsfile is our only source of truth for the required configuration.   
>#### The repo in this zip file is our only leftover we could find, it was also compromised and many untested changes were committed 
### Your mission, should  you choose to accept it - execute the following steps and fix all misconfigurations
1. Deploy a new Jenkins master using deployment playbook 
   - Jenkins' configuration uses [configuration-as-code-plugin](https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md) to start secured
   - When changing Jenkins configuration manually (UI) The CASC plugin requires manual update of the config file before restarting/redeploying Jenkins Master 
2. Deploy the minikube development environment via playbook
3. Run the Jenkinsfile and make it successfully finish
    - Connect the Jenkins to a git VCS (i.e. GitHub/bitbucket/gitlab 
      all has free account capacity) 
    - Use webhook for each commit push (no polling) 
    - Use jenkins credentials when needed (someone always hacking)
4. Run the `deploy_app_to_minikube.yml` playbook development to deploy the `greet` chart with helm to minikube development env
5. Remote stage test - ***Optional***
   - Enable remote test for the `greet` api from Jenkinsfile stage
6. SMTP - ***Optional***
   - Enable smtp mailing for post stage

### Resources:
***In the environment folder***
1. jkey - the private key for servers connect with default ec2 user
2. ips - list of ips to use with the key one for Jenkins and one for deployment

### Delivery
#### Document all your steps for the solution, so we can reuse them
#### Send us back a zip with your changes and a readme with all prerequisites and action/scripts needed to redeploy it on a clean env

- personal access token for the repo needed. supplied in secret.
- created id_rsa file with the supplied credentials
- install ansible and deploy the servers as requested, with the playbooks youv'e given me.  
- stopped jenkins server and mount the volumes with --- docker run -p 8080:8080 -p 50000:50000 -d -v /var/run/docker.sock:/var/run/docker.sock -v jenkins_home:/var/jenkins_home -v /usr/bin/docker:/usr/bin/docker jenkins:001 with same user and pass - admin & password
- making sure docker is avalibale in all users with logging to the container and chmod 666 to /var/run/docker as root user 
- NOTE: given two new servers , you'll need to create another ssh key pair and connect it to jenkins to be able to ssh and run ansible on the - minikube server, and supply the credetials name in the jenkinsfile deploy stage, under credentialsId
- added tools maven to Jenkinsfile
- changed the port names under helm deployment and service yaml files, as the app could not get started because it failed on readiness and did not created an endpoint. 
- you can also see my commits in the gitlab repo...
- for any questions, please contact me..


### The environment will self-destruct at ...


###### Note on jenkins deployment - (info only)
> Since there are problems with running docker in docker ([you can read about it here](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/))
> Please use the following host mounts in your docker configuration and not docker in docker
> as it's simpler than jenkins.io installation guide
>
```yaml
  - /var/run/docker.sock:/var/run/docker.sock
  - /usr/local/bin/docker:/usr/local/bin/docker
```
> Make sure the runtime user and group has docker socket permissions !! 

###### Helm reference - (info only)
[best practices](https://helm.sh/docs/chart_best_practices/)
[tips and tricks ](https://helm.sh/docs/howto/charts_tips_and_tricks/)
[debugging](https://helm.sh/docs/chart_template_guide/debugging/)
=======
# personetics



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/personetics1/personetics.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/personetics1/personetics/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> d03db609e439cba7b138679cc741a7965651293f
